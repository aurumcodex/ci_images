# ci_images

Some personal GitLab CI images.

- `rust_gba` is for GBA development via Rust.
- `sass_mdbook` is for websites that use plain HTML, Sass, and mdbook.
